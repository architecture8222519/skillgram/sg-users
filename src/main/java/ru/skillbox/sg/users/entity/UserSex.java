package ru.skillbox.sg.users.entity;

public enum UserSex {
    MALE,
    FEMALE
}
